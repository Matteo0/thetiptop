const BASE_URL = 'http://192.168.1.136:5173';

const MEMBER = {
  firstname: 'Jane',
  lastname: 'Doe',
  email: 'member@thetiptop.fr',
  password: 'testtest',
};

describe('Log', () => {
  it('should connect to the app and check account profil', () => {
    cy.visit(BASE_URL + '/login');
    cy.get('#email').type(MEMBER.email);
    cy.get('#password').type(MEMBER.password);
    cy.get('button[type="submit"]').click();

    cy.url().should('include', '/compte');

    cy.visit(BASE_URL + '/compte/modifier')

    cy.get('#firstname').should('have.value', MEMBER.firstname);
    cy.get('#lastname').should('have.value', MEMBER.lastname);
  })

  it('should connect and play the game and fail', () => {
    cy.visit(BASE_URL + '/login');
    cy.get('#email').type(MEMBER.email);
    cy.get('#password').type(MEMBER.password);
    cy.get('button[type="submit"]').click();

    cy.url().should('include', '/compte');

    cy.visit(BASE_URL + '/jeu-concours')

    cy.get('#code').type('123456');
    cy.get('button[type="submit"]').click();

    cy.get('.form__error').should('have.text', 'Le code doit être composé de 10 chiffres');
  })

  it('should connect and play the game and success', () => {
    cy.visit(BASE_URL + '/login');
    cy.get('#email').type(MEMBER.email);
    cy.get('#password').type(MEMBER.password);
    cy.get('button[type="submit"]').click();

    cy.url().should('include', '/compte');

    cy.visit(BASE_URL + '/jeu-concours')

    cy.get('#code').type('1234567892');
    cy.get('button[type="submit"]').click();

    cy.url().should('include', '/compte');
  })

  it('should connect and play the game and fail because code already used', () => {
    cy.visit(BASE_URL + '/login');
    cy.get('#email').type(MEMBER.email);
    cy.get('#password').type(MEMBER.password);
    cy.get('button[type="submit"]').click();

    cy.url().should('include', '/compte');

    cy.visit(BASE_URL + '/jeu-concours')

    cy.get('#code').type('1234567892');
    cy.get('button[type="submit"]').click();

    cy.get('.form__error').should('have.text', 'Ce ticket a déjà été utilisé.');
  })

  // should not acces to admin page
  it('should not access to admin page', () => {
    cy.visit(BASE_URL + '/admin');
    cy.url().should('include', '/login');
  })
})