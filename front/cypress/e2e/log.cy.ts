const BASE_URL = 'http://192.168.1.136:5173';

describe('Log', () => {
  it('should  redirect to login page if not logged in', () => {
      cy.visit(BASE_URL + '/jeu-concours');
      cy.url().should('include', '/login');
  })

  it('should  redirect to login page if not logged in', () => {
      cy.visit(BASE_URL + '/compte');
      cy.url().should('include', '/login');
  })

  it('should  redirect to login page if not logged in', () => {
      cy.visit(BASE_URL + '/admin');
      cy.url().should('include', '/login');
  })
})