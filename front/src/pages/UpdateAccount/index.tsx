import { useEffect, useState } from 'preact/hooks';
import './style.scss';
import accountsService from '../../services/accounts.service';

export function UpdateAccount() {
    const [account, setAccount] = useState({});
    const [error, setError] = useState('');

    useEffect(() => {
        accountsService.get().then((response) => {
            setAccount(response.data.data);
            console.log(response.data.data)
        })
        .catch((error) => {
            if (error.response.status === 401) {
              window.location.href = "/login";
            }
          });
    }, []);

    const onInput = (e) => {
        const { name, value } = e.target;
        const newAccount = account
        if (newAccount[name] !== undefined) newAccount[name] = value
        newAccount[name] = value
        setAccount(newAccount);
    }

    const onSubmit = async (e) => {
        e.preventDefault();
        setError('');

        if (account?.password !== account?.passwordConfirm) {
            setError('Les mots de passe ne correspondent pas');
            return;
        }

        if (account?.password?.length !== 0 && account?.password?.length < 8) {
            setError('Le mot de passe doit contenir au moins 8 caractères');
            return;
        }

        if (account?.firstname?.length < 2) {
            setError('Le prénom doit contenir au moins 2 caractères');
            return;
        }

        if (account?.lastname?.length < 2) {
            setError('Le nom doit contenir au moins 2 caractères');
            return;
        }

        const newAccount = {
            firstname: account.firstname,
            lastname: account.lastname,
        }

        if (account?.password?.length > 0) {
            newAccount.password = account.password;
        }

        const response = await accountsService.update(account.id, newAccount)
        if (response) {
            window.location.href = '/compte';
        } else {
            setError('Erreur lors de la modification');
        }
    }

	return (
		<div class="account">
			<div className="account__header">
                <div className="account__header__title">Modifier mon compte</div>
                <a href="/compte/" className="account__header__button">Annuler</a>
            </div>

            <form className="account__form form" onSubmit={onSubmit}>
                <div className="form__title">Modifier mon compte</div>
                <div className="form__group">
                    <label htmlFor="firstname" className="form__group__label">Prénom</label>
                    <input value={account.firstname} onInput={onInput} type="text" id="firstname" name="firstname" className="form__group__input"  />
                </div>
                <div className="form__group">
                    <label htmlFor="lastname" className="form__group__label">Nom</label>
                    <input value={account.lastname} onInput={onInput} type="text" id="lastname" className="form__group__input"  name="lastname" />
                </div>

                <div className="form__group">
                    <label htmlFor="password" className="form__group__label">Mot de passe</label>
                    <input value={account.password} onInput={onInput} type="password" id="password" className="form__group__input" name="password" />
                </div>

                <div className="form__group">
                    <label htmlFor="passwordConfirm" className="form__group__label">Confirmer le mot de passe</label>
                    <input value={account.passwordConfirm} onInput={onInput} type="password" id="passwordConfirm" className="form__group__input" name="passwordConfirm" />
                </div>

                { error && <div className="form__error">{ error }</div> }

                <button type="submit" className="form__button">Modifier</button>
            </form>
		</div>
	);
}