import "./style.scss";

export function Politiques() {
  return (
    <div className="politiques">
      <h1>Politique de Confidentialité</h1>
      <p>
        <strong>Dernière mise à jour :</strong> 06/05/2024
      </p>

      <p>
        Bienvenue sur notre site de jeu concours gratuit. Nous nous engageons à
        protéger la confidentialité et la sécurité de vos données personnelles.
        Cette politique de confidentialité explique quelles informations nous
        collectons, comment nous les utilisons et vos droits concernant vos
        données personnelles.
      </p>

      <h2>1. Collecte des Informations</h2>
      <p>
        Nous collectons les informations suivantes lors de votre inscription :
      </p>
      <ul>
        <li>Prénom</li>
        <li>Nom</li>
        <li>Adresse email</li>
      </ul>

      <h2>2. Utilisation des Données</h2>
      <p>Vos données personnelles seront utilisées aux fins suivantes :</p>
      <ul>
        <li>Gestion de votre participation au jeu concours.</li>
        <li>
          Envoi d'emails concernant le concours (confirmation d'inscription,
          notifications de gagnant, etc.).
        </li>
        <li>
          Envoi d'emails promotionnels et de newsletters si vous avez donné
          votre consentement.
        </li>
      </ul>

      <h2>3. Base Légale du Traitement</h2>
      <p>
        Nous traitons vos données personnelles sur la base de votre consentement
        que vous nous donnez lors de votre inscription au jeu concours.
      </p>

      <h2>4. Partage des Données</h2>
      <p>
        Nous ne partageons pas vos données personnelles avec des tiers, sauf si
        cela est nécessaire pour le respect de la loi ou avec votre consentement
        préalable.
      </p>

      <h2>5. Conservation des Données</h2>
      <p>
        Nous conservons vos données personnelles aussi longtemps que nécessaire
        pour les finalités décrites dans cette politique, sauf si une période de
        conservation plus longue est requise ou permise par la loi.
      </p>

      <h2>6. Sécurité des Données</h2>
      <p>
        Nous mettons en place des mesures de sécurité techniques et
        organisationnelles appropriées pour protéger vos données personnelles
        contre toute destruction, perte, altération, divulgation ou accès non
        autorisé.
      </p>

      <h2>7. Vos Droits</h2>
      <p>
        En vertu de la RGPD, vous disposez des droits suivants concernant vos
        données personnelles :
      </p>
      <ul>
        <li>
          <strong>Droit d'accès :</strong> Vous avez le droit de demander
          l'accès à vos données personnelles que nous détenons.
        </li>
        <li>
          <strong>Droit de rectification :</strong> Vous avez le droit de
          demander la correction de données personnelles inexactes ou
          incomplètes.
        </li>
        <li>
          <strong>Droit à l'effacement :</strong> Vous avez le droit de demander
          la suppression de vos données personnelles, sous réserve de certaines
          conditions.
        </li>
        <li>
          <strong>Droit à la limitation du traitement :</strong> Vous pouvez
          demander la limitation du traitement de vos données personnelles dans
          certains cas.
        </li>
        <li>
          <strong>Droit à la portabilité :</strong> Vous avez le droit de
          recevoir vos données personnelles dans un format structuré, couramment
          utilisé et lisible par machine et de les transmettre à un autre
          responsable de traitement.
        </li>
        <li>
          <strong>Droit d'opposition :</strong> Vous pouvez vous opposer au
          traitement de vos données personnelles à des fins de marketing direct.
        </li>
      </ul>
      <p>
        Pour exercer ces droits, veuillez nous contacter à l'adresse email
        suivante : <a href="mailto:contact@thetiptop.fr">contact@thetiptop.fr</a>
      </p>

      <h2>8. Modifications de la Politique de Confidentialité</h2>
      <p>
        Nous nous réservons le droit de modifier cette politique de
        confidentialité à tout moment. Toute modification sera publiée sur cette
        page avec une date de mise à jour. Nous vous encourageons à consulter
        régulièrement cette page pour rester informé de nos pratiques de
        protection des données.
      </p>

      <h2>9. Contact</h2>
      <p>
        Si vous avez des questions ou des préoccupations concernant cette
        politique de confidentialité ou notre traitement de vos données
        personnelles, veuillez nous contacter à :
      </p>
      <p>
        <strong>Email :</strong>{" "}
        <a href="mailto:contact@thetiptop.fr">contact@thetiptop.fr</a>
      </p>
    </div>
  );
}
