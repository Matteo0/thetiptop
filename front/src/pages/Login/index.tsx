import "../Register/style.scss";
import signImg from "../../assets/sign.png";
import Google from "../../assets/google.png";
import Facebook from "../../assets/facebook.png";
import { useState } from "preact/hooks";
import accountsService from "../../services/accounts.service";
import { setToken } from "../../utils/auth";

export function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onInput = (e) => {
    const { name, value } = e.target;
    if (name === "email") setEmail(value);
    if (name === "password") setPassword(value);
  };

  const API_URL = import.meta.env.VITE_API_URL;

  const onSubmit = async (e) => {
    e.preventDefault();
    const response = await accountsService.login(email, password);
    const token = response.data.data.token;
    setToken(token);

    if (response) {
      window.location.href = "/compte";
    } else {
      alert("Erreur lors de l'inscription");
    }
  };

  const handleFacebookSignup = async () => {
    accountsService.facebookSignup();
  };

  const handleGoogleSignup = async () => {
    accountsService.googleSignup();
  };

  return (
    <div class="login">
      <div className="login__container">
        <div className="login__container__left">
          <img src={signImg} alt="ThéTipTop" />
        </div>
        <form className="form" onSubmit={onSubmit}>
          <div className="form__title">Se connecter</div>
          <div className="form__input">
            <label for="email">Email</label>
            <input
              value={email}
              onInput={onInput}
              placeholder="Entrez votre email"
              type="email"
              id="email"
              name="email"
            />
          </div>
          <div className="form__input">
            <label for="password">Mot de passe</label>
            <input
              value={password}
              onInput={onInput}
              placeholder="Entrez votre mot de passe"
              type="password"
              id="password"
              name="password"
            />
          </div>

          <button type="submit" className="form__button">
            Se connecter
          </button>

          <a className="form__link" href="/mot-de-passe-oublie">
            Mot de passe oublié ?
          </a>
          <a className="form__link" href="/register">
            Vous n’avez pas encore de compte ? S’inscrire !
          </a>

          <div className="form__or">- OU -</div>

          <div className="form__social">
            <a
              href={API_URL + "/accounts/auth/google"}
              onClick={handleGoogleSignup}
              className="form__social__button"
            >
              <img src={Google} alt="Google" />
              Se connecter avec Google
            </a>
            <a
              href={API_URL + "/accounts/auth/facebook"}
              onClick={handleFacebookSignup}
              className="form__social__button"
            >
              <img src={Facebook} alt="Facebook" />
              Se connecter avec Facebook
            </a>
          </div>

          <div className="form__footer">ThéTipTop</div>
        </form>
      </div>
    </div>
  );
}
