import "./style.scss";

export function Mentions() {
  return (
    <div className="mentions">
      <h1>Mentions Légales</h1>

      <h2>1. Éditeur du site</h2>

      <p>
        <div>Nom de l'entreprise : ThéTipTop</div>
        <div>Adresse : 18 rue Léon Frot, 75011 Paris</div>
        <div>Téléphone : 05 01 01 01 01</div>
        <div>Email : <a href="mailto:contact@thetiptop.fr">contact@thetiptop.fr</a></div>
        <div>SIRET : 0123456789101</div>
        <div>Directeur de la publication : Mr Eric Bourdon</div>
    </p>

    <h2>2. Hébergement</h2>
    <p>
        <div>Nom de l'hébergeur : OVH</div>
        <div>Adresse : 2, rue Kellermann, 59100 Roubaix</div>
        <div>Téléphone : 1007</div>
        <div>Site web : <a href="https://www.ovh.com/">ovh.com</a></div>
    </p>

      <h2>3. Propriété intellectuelle</h2>
      <p>
        Le contenu du site (textes, images, graphismes, logo, icônes, etc.) est
        la propriété exclusive de [Nom de l'entreprise], sauf mention contraire.
        Toute reproduction, distribution, modification, adaptation,
        retransmission ou publication de ces différents éléments est strictement
        interdite sans l'accord exprès par écrit de [Nom de l'entreprise].
      </p>

      <h2>4. Collecte des données personnelles</h2>
      <p>
        Conformément à la loi Informatique et Libertés du 6 janvier 1978
        modifiée et au Règlement Général sur la Protection des Données (RGPD),
        vous disposez d’un droit d’accès, de rectification, de suppression et de
        portabilité des informations qui vous concernent. Pour exercer ces
        droits, vous pouvez nous contacter à l’adresse email suivante :{" "}
        <a href="mailto:contact@example.com">contact@example.com</a>.
      </p>

      <h2>5. Cookies</h2>
      <p>
        Le site [Nom du site] peut-être amené à vous demander l'acceptation des
        cookies pour des besoins de statistiques et d'affichage. Un cookie est
        une information déposée sur votre disque dur par le serveur du site que
        vous visitez. Il contient plusieurs données qui sont stockées sur votre
        ordinateur dans un simple fichier texte auquel un serveur accède pour
        lire et enregistrer des informations. Certaines parties de ce site ne
        peuvent être fonctionnelles sans l’acceptation de cookies.
      </p>

      <h2>6. Limitation de responsabilité</h2>
      <p>
        Les informations contenues sur ce site sont aussi précises que possible
        et le site est périodiquement remis à jour, mais peut néanmoins contenir
        des inexactitudes, des omissions ou des lacunes. Si vous constatez une
        lacune, erreur ou ce qui semble être un dysfonctionnement, merci de bien
        vouloir le signaler par email à{" "}
        <a href="mailto:contact@example.com">contact@example.com</a>, en
        décrivant le problème de la manière la plus précise possible (page
        posant problème, action déclenchante, type d’ordinateur et de navigateur
        utilisé, etc.).
      </p>

      <h2>7. Droit applicable et attribution de juridiction</h2>
      <p>
        Tout litige en relation avec l’utilisation du site [Nom du site] est
        soumis au droit français. Il est fait attribution exclusive de
        juridiction aux tribunaux compétents de [Ville du tribunal compétent].
      </p>

      <h2>8. Contact</h2>
      <p>
        Pour toute question ou information concernant le site, vous pouvez nous
        contacter à l'adresse email suivante :{" "}
        <a href="mailto:contact@example.com">contact@example.com</a>
      </p>
    </div>
  );
}
