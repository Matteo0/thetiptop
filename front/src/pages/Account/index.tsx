import "./style.scss";
import Profile from "../../assets/profile.png";
import { useEffect, useState } from "preact/hooks";
import accountsService from "../../services/accounts.service";
import participationsService from "../../services/participations.service";

export function Account() {
  const [account, setAccount] = useState({});
  const [participations, setParticipations] = useState([]);

  useEffect(() => {
    accountsService
      .get()
      .then((response) => {
        setAccount(response.data.data);

        participationsService.findAll().then((response) => {
          setParticipations(response.data.data);
        });
      })
      .catch((error) => {
        if (error.response.status === 401) {
          window.location.href = "/login";
        }
      });
  }, []);

  const formattedDate = (date) => {
    const options = { year: "numeric", month: "long", day: "numeric" };
    return new Date(date).toLocaleDateString("fr-FR", options);
  };

  return (
    <div class="account">
      <div className="account__header">
        <div className="account__header__title">Mon compte</div>
        <div className="account__header__profile">
          <img src={Profile} alt="Profil" /> {account.firstname}{" "}
          {account.lastname}
          <span style={{ fontSize: 12, opacity: 0.6 }}>({account.email})</span>
        </div>
        <a href="/compte/modifier" className="account__header__button">
          Modifier mon profil
        </a>
      </div>

      <div className="account__participations">
        <div className="account__participations__title">
          Mes participations
        </div>
        {participations.map((participation) => (
          <div className="account__participations__item part">
            <div className="part__details">
              <div className="part__details__code">
                <label>Code : </label>
                <span>{participation.numero}</span>
              </div>
              <div className="part__details__date">
                <label>Date: </label>
                {formattedDate(participation.createdAt)}
              </div>
              <div className="part__details__date">
                <label>Gain : </label>
                {participation.reward}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
