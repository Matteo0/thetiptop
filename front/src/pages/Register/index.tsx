import "./style.scss";
import signImg from "../../assets/sign.png";
import Google from "../../assets/google.png";
import Facebook from "../../assets/facebook.png";

import { useState } from "preact/hooks";
import accountsService from "../../services/accounts.service";
import { checkFormRegister } from "../../utils/register.util";

export function Register() {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const onInput = (e) => {
    const { name, value } = e.target;
    if (name === "firstname") setFirstname(value);
    if (name === "lastname") setLastname(value);
    if (name === "email") setEmail(value);
    if (name === "password") setPassword(value);
  };

  const API_URL = import.meta.env.VITE_API_URL;

  const onSubmit = async (e) => {
    e.preventDefault();
    if (checkFormRegister(firstname, lastname, email, password)) {
      const response = await accountsService.register({
        firstname,
        lastname,
        email,
        password,
      });
      if (response) {
        window.location.href = "/login";
      } else {
        alert("Erreur lors de l'inscription");
      }
    } else {
      setError("Veuillez remplir tous les champs");
    }
  };

  const handleFacebookSignup = async () => {
    accountsService.facebookSignup();
  };

  const handleGoogleSignup = async () => {
    accountsService.googleSignup();
  };

  return (
    <div class="login">
      <div className="login__container">
        <div className="login__container__left">
          <img src={signImg} alt="ThéTipTop" />
        </div>
        <form className="form" onSubmit={onSubmit}>
          <div className="form__title">Créer votre compte</div>
          <div className="form__input">
            <label for="firstname">Prénom</label>
            <input
              value={firstname}
              onInput={onInput}
              placeholder="Entrez votre prénom"
              type="text"
              id="firstname"
              name="firstname"
            />
          </div>
          <div className="form__input">
            <label for="lastname">Nom</label>
            <input
              value={lastname}
              onInput={onInput}
              placeholder="Entrez votre nom"
              type="text"
              id="lastname"
              name="lastname"
            />
          </div>
          <div className="form__input">
            <label for="email">Email</label>
            <input
              value={email}
              onInput={onInput}
              placeholder="Entrez votre email"
              type="email"
              id="email"
              name="email"
            />
          </div>
          <div className="form__input">
            <label for="password">Mot de passe</label>
            <input
              value={password}
              onInput={onInput}
              placeholder="Entrez votre mot de passe"
              type="password"
              id="password"
              name="password"
            />
          </div>

          {error && <div className="form__error">{error}</div>}

          <div style={{ fontSize: 12, width: 400, margin: 'auto', opacity: .6}}>
            En cliquant sur le bouton "S'inscrire", j'accepte que mes données personnelles (email, prénom, nom) soient collectées et utilisées conformément à la <a href="/politiques">politique de confidentialité</a> pour la gestion de ma participation au jeu concours.

          </div>

          <button type="submit" className="form__button">
            S'inscrire
          </button>

          <a className="form__link" href="/login">
            Vous n’avez pas encore de compte ? Connectez-vous !
          </a>

          <div className="form__or">- OU -</div>

          <div className="form__social">
            <a
              href={API_URL + "/accounts/auth/google"}
              onClick={handleGoogleSignup}
              className="form__social__button"
            >
              <img src={Google} alt="Google" />
              S'inscrire avec Google
            </a>
            <a
              href={API_URL + "/accounts/auth/facebook"}
              onClick={handleFacebookSignup}
              className="form__social__button"
            >
              <img src={Facebook} alt="Facebook" />
              S'inscrire avec Facebook
            </a>
          </div>

          <div className="form__footer">ThéTipTop</div>
        </form>
      </div>
    </div>
  );
}
