import "./style.scss";
import The from "../../assets/the-header.png";
import { useEffect, useState } from "preact/hooks";
import accountsService from "../../services/accounts.service";
import participationsService from "../../services/participations.service";
import { checkCodeValidity } from "../../utils/game.util";

export function Game() {
  const [account, setAccount] = useState({});
  const [code, setCode] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    accountsService
      .get()
      .then((response) => {
        setAccount(response.data.data);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          window.location.href = "/login";
        }
      });
  }, []);

  const onInput = (e) => {
    const { name, value } = e.target;
    setCode(value);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    setError("");

    if (checkCodeValidity(code) !== true) {
      setError("Le code doit être composé de 10 chiffres");
      return;
    }

    const response = await participationsService
      .create({ code })
      .catch((error) => {
        console.log(error.response);
        if (error.response.status === 409) {
          console.log(error.response.data);
          return setError(error.response.data.message);
        } else {
          setError(
            "Une erreur est survenue lors de la participation au jeu concours."
          );
        }
      });
    if (response) {
      window.location.href = "/compte";
    }
  };

  return (
    <div className="game">
      <div className="game__header">
        <div>
          <div className="game__header__title">Jeu concours</div>
          <div className="game__header__description">
            Jeu concours 100% gagant. Un infuseur à thé, des coffrets et des
            boites de 100g sont à gagner. Un tirage au sort sera effectuer avec
            tous les participants pour tenter de gagner un an de thé d’une
            valeur de 360€.
            <br /><br />
            Pour participer, saisissez le code à 10 chiffres présent sur votre ticket de caisse. Pour tout achat supérieur à 49€.
          </div>
        </div>
        <img src={The} alt="" />
      </div>
      {(account && (
        <form className="game__form form" onSubmit={onSubmit}>
          <div className="form__title">Participer</div>

          <label htmlFor="code">
            Participer en tant que {account.firstname} {account.lastname}
          </label>
          <input
            value={code}
            onInput={onInput}
            type="text"
            id="code"
            name="code"
            placeholder="Saisir le code à 10 chiffres"
          />

          <div className="form__error">{error}</div>

          <button type="submit" className="form__button">
            Valider
          </button>
        </form>
      )) || <a href="/login">Inscrivez-vous pour participer</a>}
    </div>
  );
}
