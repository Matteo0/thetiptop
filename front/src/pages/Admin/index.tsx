import { useEffect, useState } from 'preact/hooks';
import './style.scss';
import settingsService from '../../services/settings.service';
import accountsService from '../../services/accounts.service';
import participationsService from '../../services/participations.service';
import { saveAs } from 'file-saver';



export function Admin() {
	const [setting, setSetting] = useState({});
	const [accounts, setAccounts] = useState([]);
	const [participants, setParticipants] = useState([]);

	const [search, setSearch] = useState('');
	const [filteredParticipants, setFilteredParticipants] = useState([]);

	useEffect(() => {
		settingsService.get()
			.then((response) => {
				setSetting(response.data.data);
			})
			.catch ((error) => {
				if (error.response.status === 401 || error.response.status === 403) {
					window.location.href = '/login';
				}
			})

		participationsService.getAll()
			.then((response) => {
				setParticipants(response.data.data);
			})
			.catch ((error) => {
				if (error.response.status === 401 || error.response.status === 403) {
					window.location.href = '/login';
				}
			})

		accountsService.getAll()
			.then((response) => {
				setAccounts(response.data.data);
			})
			.catch ((error) => {
				if (error.response.status === 401) {
					window.location.href = '/login';
				}
			})
	}, []);

	const handleEndGame = () => {
		settingsService.update({
			endGame: true
		})
			.then((response) => {
				setSetting(response.data.data);
			})
			.catch((error) => {
				console.log(error);
			})
	}

	const handleGenerateWinner = () => {
		settingsService.update({
			generateWinner: true
		})
			.then((response) => {
				setSetting(response.data.data);
			})
			.catch((error) => {
				console.log(error);
			})
	}

	const onSearch = (event) => {
		setSearch(event.target.value);

		setFilteredParticipants(participants.filter((participant) => {
			return participant.numero.toString().includes(event.target.value.toLowerCase());
		}).slice(0, 10));
	}

	const handleDelivered = async (id) => {
		await participationsService.update(id, {
			delivered: true
		})
		
		const response = await participationsService.getAll();
		setParticipants(response.data.data);
		setFilteredParticipants(response.data.data.filter((participant) => {
			return participant.account.firstname.toLowerCase().includes(search.toLowerCase()) || participant.account.lastname.toLowerCase().includes(search.toLowerCase()) || participant.account.email.toLowerCase().includes(search.toLowerCase());
		}).slice(0, 10));		
	}

	const handleExport = () => {
		const CSVContent = ['Prenom', 'Nom', 'Email']

		participants.forEach((participant) => {
			CSVContent.push([participant.account.firstname, participant.account.lastname, participant.account.email].join(','))
		})

		const blob = new Blob([CSVContent.join('\n')], { type: 'text/csv;charset=utf-8' });
		saveAs(blob, 'participants.csv');
	}

	const accountNumber = accounts.length;
	const ticketNumber = participants.length
	const infuseurThe = participants.filter((participant) => participant.reward === 'infuseurThe').length;
	const boiteDetox = participants.filter((participant) => participant.reward === 'boiteDetox').length;
	const boiteSignature = participants.filter((participant) => participant.reward === 'boiteSignature').length;
	const coffret39 = participants.filter((participant) => participant.reward === 'coffret39').length;
	const coffret69 = participants.filter((participant) => participant.reward === 'coffret69').length;

	const percent = (value) => ((value / (ticketNumber || 1) * 100).toFixed(2)) + '%';

	return (
		<div class="admin">
			<div className="admin__container">
				<div className="flex"><div className="admin__container__title">Page d'administration</div>
					<button onClick={handleExport}>Exporter les participants</button>
				</div>
				{
					setting.finalWinnerAccount &&
					<div>
						<div>Le gagnant final est : {setting.finalWinnerAccount?.firstname} {setting.finalWinnerAccount?.lastname} ({setting.finalWinnerAccount?.email})</div>
					</div>
					|| (!setting.endGame &&
					<button onClick={handleEndGame}>Terminer le jeu concours</button>
					|| 
					<div>
						<button onClick={handleGenerateWinner}>Tirer au sort le gagant final</button>
					</div>)
				}


				<div className="admin__blocs">
					<div className="blocs__item">
						<div className="blocs__item__value">{accountNumber}</div>
						<div className="blocs__item__label">Nombre de comptes</div>
					</div>
					<div className="blocs__item">
						<div className="blocs__item__value">{ticketNumber}</div>
						<div className="blocs__item__label">Nombre de tickets</div>
					</div>
					<div className="blocs__item">
						<div className="blocs__item__value">{infuseurThe} ({percent(infuseurThe)})</div>
						<div className="blocs__item__label">Infuseur Thé</div>
					</div>
					<div className="blocs__item">
						<div className="blocs__item__value">{boiteDetox} ({percent(boiteDetox)})</div>
						<div className="blocs__item__label">Boite Detox</div>
					</div>
					<div className="blocs__item">
						<div className="blocs__item__value">{boiteSignature} ({percent(boiteSignature)})</div>
						<div className="blocs__item__label">Boite Signature</div>
					</div>
					<div className="blocs__item">
						<div className="blocs__item__value">{coffret39} ({percent(coffret39)})</div>
						<div className="blocs__item__label">Coffret 39</div>
					</div>
					<div className="blocs__item">
						<div className="blocs__item__value">{coffret69} ({percent(coffret69)})</div>
						<div className="blocs__item__label">Coffret 69</div>
					</div>
				</div>


				<div className="table__title">
					Liste des participants
				</div>
				<input className="input" type="text" placeholder="Entrez un prénom, nom, email" value={search} onChange={onSearch} />
				{filteredParticipants?.length > 0 && <table className="admin__table">
					<thead>
						<tr>
							<td>Prénom</td>
							<td>Nom</td>
							<td>Email</td>
							<td>Ticket</td>
							<td>Récompense</td>
							<td>Remis</td>
						</tr>
					</thead>
					<tbody>
						{
							filteredParticipants.map((participant) => (
								<tr>
									<td>{participant.account.firstname}</td>
									<td>{participant.account.lastname}</td>
									<td>{participant.account.email}</td>
									<td>{participant.numero}</td>
									<td>{participant.reward}</td>
									<td>{participant.delivered? 'Oui': <button onClick={(e) => handleDelivered(participant.id)}>Marqué comme remis</button>}</td>
								</tr>
							))
						}
					</tbody>
				</table> || search.length > 0 && 'Aucun résultat' || ''}

			</div>

		</div>
	);
}