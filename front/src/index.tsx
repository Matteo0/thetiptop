import { render } from 'preact';
import { LocationProvider, Router, Route } from 'preact-iso';

import { Header } from './components/Header/index.js';
import { Footer } from './components/Footer/index.js';
import { Home } from './pages/Home/index.jsx';
import { Account } from './pages/Account/index.jsx';
import { Game } from './pages/Game/index.jsx';
import { Login } from './pages/Login/index.jsx'
import { Register } from './pages/Register/index.jsx';
import { NotFound } from './pages/_404.jsx';
import { UpdateAccount } from './pages/UpdateAccount/index.jsx';
import { Admin } from './pages/Admin/index.jsx';
import { Politiques } from './pages/Politiques/index.js';
import './style.css';
import { Mentions } from './pages/Mentions/index.js';

export function App() {
	return (
		<LocationProvider>
			<Header />
			<main>
				<Router>
					<Route path="/" component={Home} />
					<Route path="/login" component={Login} />
					<Route path="/register" component={Register} />
					<Route path="/jeu-concours" component={Game} />
					<Route path="/compte" component={Account} />
					<Route path="/compte/modifier" component={UpdateAccount} />
					<Route path="/admin" component={Admin} />
					<Route path="/politiques" component={Politiques} />
					<Route path="/mentions" component={Mentions} />
					<Route default component={NotFound} />
				</Router>
			</main>
			<Footer />
		</LocationProvider>
	);
}

render(<App />, document.getElementById('app'));
