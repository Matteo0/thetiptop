import axios from 'axios'
import { getToken } from '../utils/auth'

const api = axios.create({
  baseURL: import.meta.env.VITE_API_URL
})

//add header to axios

api.interceptors.request.use(
  (config) => {
    const token = getToken()
    config.headers['x-access-token'] = token
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default api