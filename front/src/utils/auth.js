const getToken = () => {
  return document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1") || null;
}

const setToken = (token) => {
  document.cookie = `token=${token}; path=/`;
}

const removeToken = () => {
  document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

const logout = () => {
  removeToken()
  window.location.href = '/'
}

export {
  getToken,
  setToken,
  removeToken,
  logout
}