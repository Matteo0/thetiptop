import { getToken, setToken, removeToken, logout } from "./auth";

describe("auth", () => {
  beforeEach(() => {
    Object.defineProperty(document, "cookie", {
      writable: true,
      value: "",
    });
  });

  beforeAll(() => {
    delete window.location;
    window.location = { href: jest.fn() };
  });
  
  const token = "token";
  const cookie = `token=${token}; path=/`;

  it("getToken", () => {
    document.cookie = cookie;
    expect(getToken()).toBe(token);
  });

  it("setToken", () => {
    setToken(token);
    expect(document.cookie).toBe(cookie);
  });

  it("removeToken", () => {
    removeToken();
    expect(document.cookie).toBe(
      "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
    );
  });

  it("logout", () => {
    logout();
    expect(document.cookie).toBe(
      "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
    );
    expect(window.location.href).toBe("/");
  });
});
