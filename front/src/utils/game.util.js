
const checkCodeValidity = (code) => {
    if (!code) return 'Le code doit contenir 10 chiffres';
    if (code.length !== 10) return 'Le code doit contenir 10 chiffres';
    if (isNaN(code)) return 'Le code doit être composé de chiffres uniquement';

    return true;
}

export { checkCodeValidity }