

const checkFormRegister = (firstname, lastname, email, password) => {
    if (firstname.length < 2 || firstname.length > 50) {
        return 'Le prénon doit contenir entre 2 et 50 caractères'
    }

    if (lastname.length < 2 || lastname.length > 50) {
        return 'Le nom doit contenir entre 2 et 50 caractères'
    }

    const regexEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/
    if (!regexEmail.test(email)) {
        return 'L\'email n\'est pas valide'
    }

    if (password.length < 8) {
        return 'Le mot de passe doit contenir minimum 8 caractères'
    }

    return true
}

export { checkFormRegister }