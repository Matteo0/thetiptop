import { checkFormRegister } from './register.util'

describe('checkFormRegister', () => {
    it('should return error message if firstname is too short', () => {
        const result = checkFormRegister('a', 'lastname', 'email', 'password') 
        expect(result).toBe('Le prénon doit contenir entre 2 et 50 caractères')
    })   

    it('should return error message if firstname is too long', () => {
        const result = checkFormRegister('a'.repeat(51), 'lastname', 'email', 'password') 
        expect(result).toBe('Le prénon doit contenir entre 2 et 50 caractères')
    })

    it('should return error message if lastname is too short', () => {
        const result = checkFormRegister('firstname', 'a', 'email', 'password') 
        expect(result).toBe('Le nom doit contenir entre 2 et 50 caractères')
    })

    it('should return error message if lastname is too long', () => {
        const result = checkFormRegister('firstname', 'a'.repeat(51), 'email', 'password') 
        expect(result).toBe('Le nom doit contenir entre 2 et 50 caractères')
    })

    it('should return error message if email is invalid', () => {
        const result = checkFormRegister('firstname', 'lastname', 'email', 'password') 
        expect(result).toBe('L\'email n\'est pas valide')
    })

    it('should return error message if password is too short', () => {
        const result = checkFormRegister('firstname', 'lastname', 'test@email.com', 'pass') 
        expect(result).toBe('Le mot de passe doit contenir minimum 8 caractères')
    })

    it('should return true if all fields are valid', () => {
        const result = checkFormRegister('firstname', 'lastname', 'test@email.com', 'password')
        expect(result).toBe(true)
    })
})