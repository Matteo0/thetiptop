import { checkCodeValidity } from "./game.util";

describe("checkCodeValidity", () => {
    it("should return error message if code is empty", () => {
        const result = checkCodeValidity("");
        expect(result).toBe("Le code doit contenir 10 chiffres");
    });
    
    it("should return error message if code is too short", () => {
        const result = checkCodeValidity("123456789");
        expect(result).toBe("Le code doit contenir 10 chiffres");
    });
    
    it("should return error message if code is too long", () => {
        const result = checkCodeValidity("12345678901");
        expect(result).toBe("Le code doit contenir 10 chiffres");
    });
    
    it("should return error message if code contains letters", () => {
        const result = checkCodeValidity("1234a67890");
        expect(result).toBe("Le code doit être composé de chiffres uniquement");
    });
    
    it("should return true if code is valid", () => {
        const result = checkCodeValidity("1234567890");
        expect(result).toBe(true);
    });
})
