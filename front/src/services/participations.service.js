import api from "../boot/api";

class ParticipationsService {
    create (params) {
        return api.post('/participations/', params)
    }

    findAll () {
        return api.get('/participations/')
    }

    update (id, params) {
        return api.put('/participations/' + id, params)
    }

    getAll () {
        return api.get('/participations?all=true')
    }
}

export default new ParticipationsService();