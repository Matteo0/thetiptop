import api from "../boot/api";

class SettingsService {
    update (params) {
        return api.post('/settings', params)
    }

    get () {
        return api.get('/settings')
    }
}

export default new SettingsService();