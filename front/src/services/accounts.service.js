import api from "../boot/api";

class AccountsService {
    register (params) {
        return api.post('/accounts/', params)
    }

    login(email, password) {
        return api.post('/accounts/login', {
            email,
            password
        })
    }

    getAll () {
        return api.get('/accounts/')
    }

    get () {
        return api.get('/accounts/me')
    }

    isAdmin () {
        return api.get('/accounts/is-admin')
    }

    update (id, account) {
        return api.post('/accounts/' + id, {
            ...account
        })
    }

    facebookSignup() {
        return api.get('/accounts/auth/facebook')
    }

    googleSignup() {
        return api.get('/accounts/auth/google')
    }
}

export default new AccountsService();