import logo2x from '../../assets/logo2x.png'
import Facebook from '../../assets/facebook.png'
import Instagram from '../../assets/instagram.png'

import './style.scss';

export function Footer() {
	return (
		<footer className="footer">
			<div className="footer__container">
				<div className="footer__container__item">
					<img className="footer__container__item__img" src={ logo2x } alt="ThéTipTop" />
				</div>
				<div className="footer__container__links">
					<div className="footer__container__item">
						<a className="footer__container__item__link" href="/">
							Accueil
						</a>
						<a className="footer__container__item__link" href="/jeu-concours">
							Jeu concours
						</a>
						<a className="footer__container__item__link" href="/compte">
							Mon Compte
						</a>
					</div>
					<div className="footer__container__item">
						<a className="footer__container__item__link" href="/politiques">
							Politiques de confidentialité
						</a>
						<a className="footer__container__item__link" href="/mentions">
							Mentions légales
						</a>
					</div>	
				</div>	
			</div>

			<div className="footer__end">
				<div></div>
				<div className="copyright">
					&copy; ThéTipTop 2024
				</div>
				<div className="footer__end__social">
					<a href="https://www.facebook.com/">
						<img src={ Facebook } alt="Facebook" />
					</a>
					<a href="https://www.instagram.com/">
						<img src={ Instagram } alt="Instagram" />
					</a>
				</div>
			</div>
		</footer>
	);
}
