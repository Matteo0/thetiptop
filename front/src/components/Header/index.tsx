import logo from '../../assets/logo.png'

import './style.scss';

export function Header() {
	return (
		<header className="header">
			<div className="header__logo">
				<img className="header__logo__img" src={ logo } alt="ThéTipTop" />
			</div>
			<nav className="header__nav">
				<a className="header__nav__item" href="/">
					Accueil
				</a>
				<a className="header__nav__item" href="/jeu-concours">
					Jeu concours
				</a>
				<a className="header__nav__item" href="/compte">
					Mon Compte
				</a>
			</nav>
		</header>
	);
}
