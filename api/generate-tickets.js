const { PrismaClient } = require("@prisma/client");
const { getReward } = require("./src/controllers/participations.controller");
const prisma = new PrismaClient();
const NB_TICKETS = 50;

async function main() {
  const accounts = await prisma.account.findMany();
  for (let i = 0; i < accounts.length; i++) {
    const account = accounts[i];
    // const account = await prisma.account.create({
    //   data: {
    //     email: `user${i}@example.com`,
    //     active: true,
    //     firstname: `Firstname${i}`,
    //     lastname: `Lastname${i}`,
    //     role: 'MEMBER',
    //   },
    // });

    for (let j = 0; j < NB_TICKETS; j++) {
      let numero = Math.floor(Math.random() * 10000000000);
      await prisma.ticket.create({
        data: {
          numero: numero.toString().padStart(10, "0"),
          accountId: account.id,
          reward: await getReward(),
        },
      });
    }
  }

  console.log(accounts.length + " comptes actifs avec " + NB_TICKETS + " tickets chacun ont été créés.");
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
