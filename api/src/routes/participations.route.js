const auth = require("../middlewares/auth");


module.exports = (app) => {
  const controller = require("../controllers/participations.controller");
  var router = require("express").Router();

  router.get("/", [auth.verifyToken], controller.findAll);
  router.post("/", [auth.verifyToken], controller.create);
  router.put('/:id', [auth.verifyToken], controller.update);

  app.use("/participations", router);

}