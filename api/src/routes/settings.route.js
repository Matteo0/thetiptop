const auth = require("../middlewares/auth");


module.exports = (app) => {
  const controller = require("../controllers/settings.controller.js");
  var router = require("express").Router();

  router.get('/', [auth.verifyToken, auth.isAdmin], controller.read);
  router.post("/", [auth.verifyToken, auth.isAdmin], controller.update);

  app.use("/settings", router);

}