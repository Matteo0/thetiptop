const passport = require('../config/passport.config');
const auth = require("../middlewares/auth");
const jwt = require("jsonwebtoken");


module.exports = (app) => {
  const controller = require("../controllers/accounts.controller");
  var router = require("express").Router();

  router.get("/", [auth.verifyToken, auth.isEmployee], controller.getAll);
  router.post("/", controller.create);
  
  router.get("/me", [auth.verifyToken], controller.readMe);
  router.get("/is-admin", [auth.verifyToken, auth.isAdmin], controller.readMe);

  router.post("/login", controller.login);
  
  router.get("/auth/facebook", passport.authenticate("facebook", { scope: ["email"] }))
  router.get('/facebook/callback', passport.authenticate('facebook', { failureRedirect: `${process.env.APP_URL}/login?error=facebook`}), (req, res) => {
    const token = jwt.sign({ id: req.user.id }, process.env.JWT_SECRET, { expiresIn: '24h' });
    res.cookie('token', token, { httpOnly: false, secure: true });
    res.redirect(process.env.APP_URL + '/compte');
  })

  router.get("/auth/google", passport.authenticate("google", { scope: ["profile", "email"] }));
  router.get('/google/callback', passport.authenticate('google', { failureRedirect: `${process.env.APP_URL}/login?error=google`}), (req, res) => {
    console.log("GOOGLE CALLBACK");
    console.log(req.user);
    const token = jwt.sign({ id: req.user.id }, process.env.JWT_SECRET, { expiresIn: '24h' });
    res.cookie('token', token, { httpOnly: false, secure: true });
    res.redirect(process.env.APP_URL + '/compte');
  })

  router.post("/logout", [auth.verifyToken, auth.adminOrMe], controller.logout);
  router.post(
    "/request-activation",
    [auth.verifyToken],
    controller.requestActivation
  );
  router.post("/check-activation", controller.checkActivation);
  router.post("/activate", controller.activate);
  router.post("/request-reset", controller.requestReset);
  router.post("/check-reset", controller.checkReset);
  router.post("/reset", controller.reset);

  router.get("/:id", [auth.verifyToken], controller.get);
  router.post("/:id", [auth.verifyToken], controller.update);
  router.delete("/:id", [auth.verifyToken, auth.isAdmin], controller.delete);

  app.use("/accounts", router);
};
