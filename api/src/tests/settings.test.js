const request = require("supertest");
const app = require("../../index")(false)
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

describe("Tests Controllers Settings", () => {
  let authToken;

  beforeAll(async () => {
    const res = await request(app).post("/accounts/login").send({
      email: "admin@thetiptop.fr",
      password: "testtest",
    });
    authToken = res.body.data.token;
  });

  afterAll(async () => {
    await prisma.$disconnect();
  });

  it("should create settings", async () => {
    const res = await request(app)
      .post("/settings")
      .set("x-access-token", authToken)
      .send();
    expect(res.statusCode).toEqual(200);
  })


  it("should get settings", async () => {
    const res = await request(app)
      .get("/settings")
      .set("x-access-token", authToken)
      .send();
    expect(res.statusCode).toEqual(200);
  });

    it("should update settings", async () => {
        const res = await request(app)
        .post("/settings")
        .set("x-access-token", authToken)
        .send({
            generateWinner: true
        });
        expect(res.statusCode).toEqual(200);
    });
});
