const request = require("supertest");
const app = require("../../index")(false)
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

describe("Tests Controllers Settings", () => {
  let authToken;

  beforeAll(async () => {
    const res = await request(app).post("/accounts/login").send({
      email: "admin@thetiptop.fr",
      password: "testtest",
    });
    authToken = res.body.data.token;
  });

  afterAll(async () => {
    await prisma.$disconnect();
  });

  const email = "test@thetiptop.fr";
  const password = "testtest";
  let accountId = '';

  it("should create account", async () => {
    const res = await request(app)
      .post("/accounts")
      .set("x-access-token", authToken)
      .send({
        email,
        password,
        firstname: "test",
        lastname: "test",
        role: "MEMBER",
        active: false,
      });


    accountId = res.body.data.id;
    console.log('***** Account Id *******', accountId)
    expect(res.statusCode).toEqual(200);
  });

  it("should get accounts", async () => {
    const res = await request(app)
      .get("/accounts")
      .set("x-access-token", authToken)
      .send();
    expect(res.statusCode).toEqual(200);
  });


  it("should delete account", async () => {
    const res = await request(app)
      .delete("/accounts/" + accountId)
      .set("x-access-token", authToken)
      .send();
    expect(res.statusCode).toEqual(200);
  });

});
