const Prisma = require('@prisma/client')
const { TokenExpiredError } = require('jsonwebtoken')

exports.PayloadError = class PayloadError extends Error {
  constructor(message) {
    super(message)
    this.name = 'PayloadError'
  }
}

exports.catchErrors = (err, res) => {
  console.log('catchErrors', err)
  if (typeof res === 'function') return res(null, err)
  if (!res) return err
  let response = {
    result: 'error'
  }
  if (err instanceof SyntaxError) {
    res.status(400)
    response.code = 'BAD_REQUEST'
  } else if (err instanceof this.PayloadError) {
    res.status(400)
    response.code = 'BAD_REQUEST'
  } else if (err instanceof Prisma.PrismaClientValidationError) {
    res.status(400)
    response.code = 'PRISMA_CLIENT_VALIDATION_ERROR'
    response.message = err.message
  } else if (err instanceof TokenExpiredError) {
    res.status(401)
    response.code = 'TOKEN_EXPIRED'
  } else if (err.code === 'ACCOUNT_NOT_ACTIVE') {
    res.status(401)
    response.code = 'ACCOUNT_NOT_ACTIVE'
  } else if (err.code === 'NO_TOKEN') {
    res.status(401)
    response.code = 'NO_TOKEN'
  } else {
    res.status(err.status || 500)
    response.code = err.code || 'INTERNAL_SERVER_ERROR'
  }

  response.name = err.name
  response.message = err.message
  if (process.env.ENVIRONMENT !== 'production') {
    response.stack = err.stack
  }
  //replace new lines in message to make it readable in json
  if (response.message) response.message = response.message.replace(/(?:\r\n|\r|\n)/g, ' ')
  return res.json(response)
}

exports.sendData = (data, res, options = {}, status = 200, config) => {
  if (typeof res === 'function') return res(data)
  if (!res) return data
  if (!data) return this.catchErrors({ code: 'NOT_FOUND', message: 'Not found' }, res)
  return res.status(status).json({
    result: 'success',
    count: data.length,
    ...options,
    data
  })
}
