const nodemailer = require("nodemailer")
const Handlebars = require("handlebars")
const CONF = require("../config/emails.config")
const fs = require("fs")

const send = (from, to, bcc, subject, html, attachments, replyTo = null) => {
  if (CONF.DEBUG_RECEIVER) {
    console.log("[EMAIL DEBUG] FORCE TO: <" + CONF.DEBUG_RECEIVER + ">")
    to = CONF.DEBUG_RECEIVER
  }
  const transporter = nodemailer.createTransport({
    host: CONF.HOST,
    port: CONF.PORT,
    secure: CONF.SECURE,
    auth: {
      user: CONF.LOGIN,
      pass: CONF.PASSWORD,
    },
    //dkim: CONF.DKIM
  })

  if (process.env.ENVIRONNEMENT !== "production") {
    subject = "[TEST] " + subject
  }
  const mailOptions = {
    from,
    to,
    bcc,
    subject,
    html,
    attachments,
    replyTo,
  }
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log("[EMAIL ERROR]", error)
    } else {
      console.log("[EMAIL SENT] TO : <" + to + "> SUBJECT : " + subject)
    }
  })
  // return html
}

exports.sendEmail = (config = {}) => {
  try {
    var from, to, bcc, subject, html, attachments, replyTo
    if (!config?.to) {
      return console.log("[EMAIL ERROR] Config required <to> parameter")
    } else {
      to = config.to
    }

    if (!config?.replyTo) {
      replyTo = CONF.REPLY_TO
    } else {
      replyTo = config.replyTo
    }

    if (config?.bcc) {
      bcc = config.bcc
    }

    if (!config?.from) {
      from = CONF.FROM
    } else {
      from = config.from
    }

    if (!config?.type) {
      return console.log("[EMAIL ERROR] Config required <type> parameter")
    }
    if (typeof config.data === "undefined") config.data = {}

    var type, template

    try {
      type = require("../config/emails/types/" + config.type)
    } catch (e) {
      return console.log("[EMAIL ERROR] Unknown email type <" + config.type + ">")
    }
    subject = type.subject
    try {
      // read file from template folder
      template = Handlebars.compile(fs.readFileSync(__dirname + "/../config/emails/templates/" + type.template.file, "utf8"))
    } catch (e) {
      console.log(e)
      return console.log("[EMAIL ERROR] Unknown email template <" + type.template.file + ">")
    }
    if (type?.data?.subject?.length > 0) {
      for (var i = 0; i < type.data.subject.length; i++) {
        var _var = type.data.subject[i]
        if (!config.data || typeof config.data[_var] === "undefined") {
          return console.log("[EMAIL ERROR] Data <" + _var + "> is required for email type <" + config.type + ">")
        }
      }
    }
    if (type?.data?.body?.length > 0) {
      for (var i = 0; i < type.data.body.length; i++) {
        var _var = type.data.body[i]
        if (!config.data || typeof config.data[_var] === "undefined") {
          return console.log("[EMAIL ERROR] Data <" + _var + "> is required for email type <" + config.type + ">")
        }
      }
    }

    html = template(config.data)

    send(from, to, bcc, subject, html, attachments, replyTo)
  } catch (e) {
    return console.log("[EMAIL ERROR] ", e)
  }
}
