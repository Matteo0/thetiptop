const { createClient } = require("redis");
const jwt = require("jsonwebtoken");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const redis = createClient();
if (!process.env.REDIS_DISABLED) {
  redis.connect();
  redis.on("error", (err) => {
    console.log("Redis error: ", err);
  });
}

const crypto = require("crypto");

const hashPassword = (password) =>
  crypto
    .createHash("sha256")
    .update("thetiptop_password_fe3DD496l701")
    .update(password)
    .digest("hex");

exports.hashPassword = hashPassword;

exports.generateToken = (
  id,
  expiresIn = parseInt(process.env.JWT_EXPIRES_IN)
) => {
  const token = jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn,
  });
  if (process.env.REDIS_DISABLED) return token;
  redis.set(id, token);
  redis.expire(id, expiresIn);
  return token;
};

exports.revokeToken = async (token) => {
  if (process.env.REDIS_DISABLED) return true;
  const decoded = jwt.decode(token);
  return decoded ? await redis.del(decoded.id) : false;
};

exports.checkAccountToken = async (id, token) => {
  if (process.env.REDIS_DISABLED) return true;
  return token === (await redis.get(id));
};

exports.init = async (cb) => {
  await prisma.Account.createMany({
    data: [
      {
        email: "admin@thetiptop.fr",
        password: this.hashPassword("testtest"),
        role: "ADMIN",
        active: true,
        firstname: "Mattéo",
        lastname: "Kocken",
      },
      {
        email: "employee@thetiptop.fr",
        password: this.hashPassword("testtest"),
        role: "EMPLOYEE",
        active: true,
        firstname: "John",
        lastname: "Doe",
      },
      {
        email: "member@thetiptop.fr",
        password: this.hashPassword("testtest"),
        role: "MEMBER",
        active: true,
        firstname: "Jane",
        lastname: "Doe",
      },
    ],
    skipDuplicates: true,
  });

  if (cb) cb();
};
