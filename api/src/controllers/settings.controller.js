const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient();
const { sendData, catchErrors } = require("../utils/responses.utils");

exports.read = async (req, res) => {
  try {
    const setting = await prisma.Setting.findFirst()
    if (setting?.finalWinnerAccountId) {
      setting.finalWinnerAccount = await prisma.account.findFirst({
        where: {
          id: setting.finalWinnerAccountId
        }
      })
    }
    return sendData(setting, res)
  } catch (err) {
    return catchErrors(err, res)
  }
};

exports.update = async (req, res) => {
  try {

    if (req.body.generateWinner) {
      // get all account with at least 1 ticket
      const accounts = await prisma.account.findMany({
        where: {
          tickets: {
            some: {
              reward: {
                not: null
              }
            }
          }
        }
      })
      
      const randomIndex = Math.floor(Math.random() * accounts.length);
      const winner = accounts[randomIndex]

      delete req.body.generateWinner
      req.body.finalWinnerAccountId = winner.id
    }

    let setting = await prisma.Setting.findFirst({
        where: {
            id: '1'
        }
    })

    if (setting) {
        setting = await prisma.Setting.update({
            where: {
                id: setting.id
            },
            data: {
                ...req.body
            }
        })
        return sendData(setting, res)
    }
    setting = await prisma.Setting.create({
        data: {
          id: '1',
            ...req.body
        }
    })

    return sendData(setting, res)
  } catch (err) {
    return catchErrors(err, res)
  }
};