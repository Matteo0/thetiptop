const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient();
const { sendData, catchErrors } = require("../utils/responses.utils");


const getReward = async () => {
  const limitTickets = 1500000;
  const probabilities = {
    infuseurThe: 0.6,
    boiteDetox: 0.2,
    boiteSignature: 0.1,
    coffret39: 0.06,
    coffret69: 0.04
  };

  const totalRewardAttribute = await prisma.ticket.groupBy({
    by: ['reward'],
    _count: true,
  });

  let distribute = {};
  totalRewardAttribute.forEach(ticket => {
    distribute[ticket.reward] = ticket._count;
  });

  const totalTicketsDistributed = Object.values(distribute).reduce((acc, current) => acc + current, 0);
  const ticketsRemaining = limitTickets - totalTicketsDistributed;

  let adjustedProbabilities = {};
  let totalAdjustedProbability = 0;
  for (let prize in probabilities) {
    const expectedTotal = Math.floor(limitTickets * probabilities[prize]);
    const remaining = expectedTotal - (distribute[prize] || 0);
    const adjustedProbability = remaining / ticketsRemaining;
    adjustedProbabilities[prize] = adjustedProbability > 0 ? adjustedProbability : 0;
    totalAdjustedProbability += adjustedProbabilities[prize];
  }

  const rand = Math.random() * totalAdjustedProbability;
  let cumul = 0;
  for (let [prize, proba] of Object.entries(adjustedProbabilities)) {
    cumul += proba;
    if (rand <= cumul) {
      return prize;
    }
  }

  return 'infuseurThe';
};

exports.getReward = getReward;

exports.create = async (req, res) => {
  try {
    const totalCount = await prisma.ticket.count();
    if (totalCount >= 1500000) {
      return res.status(400).json({ message: 'Les tickets sont épuisés.' });
    }

    // Limite à 10 tickets par utilisateur par jour
    const count = await prisma.ticket.count({
      where: {
        accountId: req.account.id,
        createdAt: {
          gte: new Date(new Date().setHours(0,0,0,0)),
          lt: new Date(new Date().setHours(23,59,59,999))
        }
      }
    });

    const isNumeroExist = await prisma.ticket.findFirst({
      where: {
        numero: req.body.code
      }
    });

    if (isNumeroExist) {
      return res.status(409).json({ message: 'Ce ticket a déjà été utilisé.' });
    }

    if (count >= 10) {
      return res.status(400).json({ message: 'Vous avez atteint le nombre maximum de tickets pour aujourd\'hui. Revenez demain.' });
    }
    const rewardName = await getReward(); 
    
    const ticket = await prisma.ticket.create({
      data: {
        numero: req.body.code,
        accountId: req.account.id,
        reward: rewardName, 
      }
    });

    return sendData(ticket, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.findAll = async (req, res) => {
  const where = {
    accountId: req.account.id
  }
  if (req.query.all && (req.account.role === 'ADMIN' || req.account.role === 'EMPLOYEE')) {
    delete where.accountId
  }

  try {
    const accounts = await prisma.Ticket.findMany({
      where,
      include: {
        account: true
      }
    })
    return sendData(accounts, res)
  } catch (err) {
    return catchErrors(err, res)
  }
}

exports.update = async (req, res) => {
  try {
    let id = req.params.id;

    const account = await prisma.Ticket.update({
      where: {
        id: id,
      },
      data: {
        delivered: req.body.delivered,
      },
    });

    return sendData(account, res);
  } catch (err) {
    return catchErrors(err, res);
  }
}