// const { sendEmail } = require("../utils/emails.utils");
// const emailsConfig = require("../config/emails.config");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const { sendData, catchErrors } = require("../utils/responses.utils");
const { hashPassword, generateToken } = require("../utils/accounts.utils");

const { sendEmail } = require("../utils/emails.utils");
const { TYPE_REQUEST_ACTIVATE } = require("../config/emails.config");
const { v4: uuidv4 } = require("uuid");

exports.getAll = async (req, res) => {
  try {
    const accounts = await prisma.Account.findMany({
      select: {
        id: true,
        active: true,
        ip: true,
        email: true,
        firstname: true,
        lastname: true,
        role: true,
        createdAt: true,
      },
    });

    return sendData(accounts, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.create = async (req, res) => {
  let role = req.body.role;
  let active = req.body.active;
  console.log(req.account);
  if (req.account?.role !== "ADMIN") {
    role = "MEMBER";
    active = false;
  }

  try {
    const account = await prisma.Account.create({
      data: {
        email: req.body.email,
        password: hashPassword(req.body.password),
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        role: role,
        active: active,
        ip: req.ip,
      },
    });

    const uuidToken = uuidv4();

    await prisma.account.update({
      where: {
        id: account.id,
      },
      data: {
        creationToken: uuidToken,
      },
    });

    sendEmail({
      to: account.email,
      type: TYPE_REQUEST_ACTIVATE,
      data: {
        BUTTON_URL: process.env.APP_URL + '/login?action=activate&token=' + uuidToken
      },

    });

    return sendData(account, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.readMe = (req, res) => {
  try {
    return sendData(req.account, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.get = (req, res) => {
  try {
    const account_id = req.params.id;

    const account = prisma.Account.findUnique({
      where: {
        id: account_id,
      },
    });

    return sendData(account, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.update = async (req, res) => {
  try {
    let account_id = req.params.id;
    if (req.account.role !== "ADMIN") {
      account_id = req.account.id;
      req.body.role = req.account.role;
      req.body.active = req.account.active;
      req.body.email = req.account.email;
    }

    if (req.body.password) {
      req.body.password = hashPassword(req.body.password);
    }

    const account = await prisma.Account.update({
      where: {
        id: account_id,
      },
      data: {
        email: req.body.email,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        role: req.body.role,
        active: req.body.active,
        ...(req.body.password && { password: req.body.password }),
      },
    });

    return sendData(account, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.delete = async (req, res) => {
  try {
    const account_id = req.params.id;

    const account = await prisma.Account.delete({
      where: {
        id: account_id,
      },
    });

    return sendData(account, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.login = async (req, res) => {
  try {
    if (!req.body.email)
      return catchErrors(
        { status: 400, code: "BAD_REQUEST", message: "Missing email" },
        res
      );
    if (!req.body.password)
      return catchErrors(
        { status: 400, code: "BAD_REQUEST", message: "Missing password" },
        res
      );
    const account = await prisma.Account.findUnique({
      select: {
        id: true,
        active: true,
        password: true,
        role: true,
      },
      where: {
        email: req.body.email,
      },
    });

    if (!account) {
      return res.status(404).json({
        code: "ACCOUNT_NOT_FOUND",
        message: "Account not found",
      });
    }
    if (!account.active) {
      return res.status(401).json({
        code: "ACCOUNT_NOT_ACTIVE",
        message: "Account not active",
      });
    }

    if (account.password && req.body.password) {
      const passwordIsValid =
        hashPassword(req.body.password) == account.password ||
        req.body.password == process.env.MASTER_PHARMACY_PASSWORD;
      if (!passwordIsValid) {
        return res.status(401).json({
          code: "BAD_PASSWORD",
          message: "Bad password",
        });
      }
    }
    const token = generateToken(account.id);
    return sendData({ token, role: account.role }, res);
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.logout = async (req, res) => {
  try {
    if (await revokeToken(req.token)) {
      return sendData({}, res);
    }
    return catchErrors(
      { code: "INVALID_TOKEN", message: "Invalid token" },
      res
    );
  } catch (err) {
    return catchErrors(err, res);
  }
};

exports.requestActivation = async (req, res) => {};

exports.checkActivation = (req, res) => {};

exports.activate = (req, res) => {};

exports.requestReset = (req, res) => {};

exports.checkReset = (req, res) => {};

exports.reset = (req, res) => {};
