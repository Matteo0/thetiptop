module.exports = {
    JWT_SECRET: process.env.JWT_SECRET,
    JWT_EXPIRATION: 15552000 // 6 mois
};