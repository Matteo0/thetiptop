module.exports = {
  subject: "ThéTipTop - Activez votre compte",

  template: {
    name: "default",
    file: "request_activate.hbs",
    variables: {
      TITLE: "Créer votre mot de passe",
      CONTENT:
        "<p>Bienvenue sur ThéTipTop !</p><p>Vous venez de vous créer un compte.<br/>Pour l'activer rendez-vous à l'adresse suivante : {{BUTTON_URL}} ou cliquez sur le bouton ci-dessous</p>",
      BUTTON_TEXT: "Activer mon compte",
    },
  },

  variables: {
    body: ["url"],
  },
}