module.exports = {
  subject: "ThéTipTop - Réinitialisez votre mot de passe",

  template: {
    name: "default",
    file: "request_reset.hbs",

    variables: {
      TITLE: "Réinitialisez votre mot de passe ThéTipTop",
      CONTENT:
        "<p>Vous avez demandé la réinitialisation de votre mot de passe.<br/>Pour définir un nouveau mot de passe, rendez-vous à l'adresse suivante : {{BUTTON_URL}} ou cliquez sur le bouton ci-dessous</p>",
      BUTTON_TEXT: "Réinitialisez votre mot de passe",
    },
  },

  variables: {
    body: ["url", "CONTACT_EMAIL"],
  },
}