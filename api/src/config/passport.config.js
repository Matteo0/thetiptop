const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient();

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (obj, done) => {
  const id = obj.id || obj;
  const user = await prisma.Account.findUnique({ where: { id } });
  done(null, user);
});

passport.use(new GoogleStrategy({
  clientID: '890560064788-t9pklp394j7ggjmvtta7n3oaloc044rp.apps.googleusercontent.com',
  clientSecret: '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',
  callbackURL: '/accounts/google/callback',
},
async (accessToken, refreshToken, profile, done) => {
    let account = await prisma.account.findUnique({
      where: {
        email: profile.emails[0].value,
      },
    });

    if (!account) {
      console.log(profile)
      const email = profile.emails[0].value || `${ profile.id }@gmail.com`
      account = await prisma.account.create({
        data: {
          email,
          firstname: profile.displayName,
          provider: 'google',
          providerId: profile.id,
          password: '',
          active: true,
        },
      });
    } else {
      await prisma.account.update({
        where: {
          id: account.id,
        },
        data: {
          provider: 'google',
          providerId: profile.id,
        },
      });
    }

    profile.id = account.id

    done(null, profile);
}));

passport.use(new FacebookStrategy({
  clientID: '645675924310635',
  clientSecret: '1bfb470f8108d861cf36db52347c338e',
  callbackURL: '/accounts/facebook/callback',
  profileFields: ['id', 'emails', 'name'] 
},
async (accessToken, refreshToken, profile, done) => {
  console.log(profile)
  let account = await prisma.account.findUnique({
    where: {
      email: profile.emails[0].value,
    },
  });

  if (!account) {
    account = await prisma.account.create({
      data: {
        email: profile.emails[0].value,
        firstname: profile.name.givenName,
        lastname: profile.name.familyName,
        provider: 'facebook',
        providerId: profile.id,
        password: '',
        active: true,
      },
    });
  } else {
    await prisma.account.update({
      where: {
        id: account.id,
      },
      data: {
        provider: 'facebook',
        providerId: profile.id,
      },
    });
  }

  profile.id = account.id

  done(null, profile);
}));

module.exports = passport;