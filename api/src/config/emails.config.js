module.exports = {
  HOST: process.env.EMAIL_HOST,
  PORT: process.env.EMAIL_PORT,
  SECURE: process.env.EMAIL_SECURE,
  LOGIN: process.env.EMAIL_LOGIN,
  PASSWORD: process.env.EMAIL_PASSWORD,
  FROM: process.env.EMAIL_SENDER,

  DEBUG_RECEIVER: process.env.EMAIL_DEBUG_RECEIVER,

  DKIM: {
    domainName: process.env.EMAIL_DKIM_DOMAIN,
    keySelector: process.env.EMAIL_DKIM_KEY_SELECTOR,
    privateKey: process.env.EMAIL_DKIM_PRIVATE_KEY,
  },

  TYPE_REQUEST_RESET: "request_reset",
  TYPE_REQUEST_ACTIVATE: "request_activate",
}
  