const jwt = require('jsonwebtoken')
const { PrismaClient } = require('@prisma/client')
const { catchErrors } = require('../utils/responses.utils')
const { checkAccountToken } = require('../utils/accounts.utils')

const prisma = new PrismaClient()

const verifyToken = async (req, res, next) => {
  req.token = req.headers['x-access-token']

  if (!req.token) {
      req.token = req.query.token
  }

  if (!req.token) {
      return catchErrors({ code: 'NO_TOKEN', message: 'No token provided' }, res)
  }

  const where = {}

  try {
      const decoded = jwt.verify(req.token, process.env.JWT_SECRET)
      if (!decoded || !(await checkAccountToken(decoded.id, req.token))) {
          return catchErrors({ code: 'INVALID_TOKEN', message: 'Invalid token' }, res)
      }
      where.id = decoded.id.toString()
  } catch (err) {
      return catchErrors(err, res)
  }

  prisma.Account.findUnique({
      select: {
          id: true,
          email: true,
          firstname: true,
          lastname: true,
          active: true,
          role: true,
          password: true,
      },
      where
  })
      .then(async (account) => {
          if (account && (account.active || !account.password)) {
              req.account = account
              try {
                  delete req.account.password
                  delete req.account.active
              } catch (err) {
                  return catchErrors(err, res)
              }
              return next()
          } else {
              return catchErrors({ code: 'ACCOUNT_NOT_ACTIVE', message: 'Account not active' }, res)
          }
      })
      .catch(err => catchErrors(err, res))
}

const isAdmin = (req, res, next) => {
  if (req.account.role !== "ADMIN") {
    return res.status(403).send({
      code: "NO_PERMISSION",
      message: "No permission",
    });
  }
  next();
};

const isEmployee = (req, res, next) => {
  if (req.account.role !== "ADMIN" && req.account.role !== "EMPLOYEE") {
    return res.status(403).send({
      code: "NO_PERMISSION",
      message: "No permission",
    });
  }
  next();
};

const adminOrMe = (req, res, next) => {
  if (req.account.role !== "ADMIN" && req.account.id !== req.params.id) {
    return res.status(403).send({
      code: "NO_PERMISSION",
      message: "No permission",
    });
  }
  next();
};

const auth = {
  verifyToken,
  isAdmin,
  isEmployee,
  adminOrMe
};
module.exports = auth;
