const { PrismaClient } = require("@prisma/client");
const { getReward } = require("./src/controllers/participations.controller");
const prisma = new PrismaClient();

const NB_ACCOUNTS = 500;

async function main() {
  
  for (let i = 0; i < NB_ACCOUNTS; i++) {
    await prisma.account.create({
      data: {
        email: `user${i}@example.com`,
        active: true,
        firstname: `Firstname${i}`,
        lastname: `Lastname${i}`,
        role: 'MEMBER',
      },
    });
  }

  console.log(NB_ACCOUNTS + " comptes créés.");
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
