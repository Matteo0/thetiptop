const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require('./src/config/passport.config');
const session = require('express-session');

if (process.env.ENVIRONMENT !== "production") {
  require("dotenv").config();
}

const app = express();

app.set('trust proxy', true);


require('./src/utils/accounts.utils').init()

var corsOptions = {
  origin: true,
  credentials: true,
};

app.use(session({
  secret: '4grz8g4zre4greg',
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(cors(corsOptions));

app.use(
  bodyParser.json({
    limit: "50mb",
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
  })
);

// Simple Welcome Route
if (process.env.ENVIRONMENT !== "production") {
  app.get("/", (req, res) => {
    return res.json({
      message: "Welcome to API.",
    });
  });
}

app.set("json spaces", "  ");
// Simple logger
app.get("*", (req, res, next) => {
  console.log("GET", req.params[0], req.query);
  res.setHeader("Content-Type", "application/json");
  next();
});

app.post("*", (req, res, next) => {
  console.log("POST", req.params[0], req.query, req.body);
  res.setHeader("Content-Type", "application/json");
  next();
});

app.put("*", (req, res, next) => {
  console.log("PUT", req.params[0], req.query, req.body);
  res.setHeader("Content-Type", "application/json");
  next();
});

app.delete("*", (req, res, next) => {
  console.log("DELETE", req.params[0], req.query);
  res.setHeader("Content-Type", "application/json");
  next();
});

app.use((req, res, next) => {
  for (let key in req.query) {
    if (req.query[key] === "null") req.query[key] = null;
  }
  next();
});

require("fs")
  .readdirSync(require("path").join(__dirname, "src", "routes"))
  .forEach(function (file) {
    require("./src/routes/" + file)(app);
  });


module.exports = function (run_server = true) {
  if (run_server) {
    const PORT = process.env.PORT || 5000;
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}.`);
    });
  }
  return app;
}

module.exports(true)